ARG GHC_VERSION
FROM haskell:${GHC_VERSION}

ARG HLS_RESOLVER
ARG GHC_VERSION=${GHC_VERSION}

RUN apt-get update \
  && apt-get install -y libicu-dev libncurses-dev libgmp-dev zlib1g-dev \
  && git clone https://github.com/haskell/haskell-language-server --recurse-submodules

RUN cd haskell-language-server \
  && stack --resolver=${HLS_RESOLVER} --install-ghc ./install.hs hie-${GHC_VERSION} \
  && apt-get autoclean -y \
  && rm -rf /var/lib/apt/lists/*

VOLUME /root/.stack